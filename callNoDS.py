#!/usr/bin/python

'''
==============================================================================================================
Author	: Manasa Ramakrishna
Date	: 5th December, 2017
Aim 	: This script takes an input file containing one or more protein sequences
		  and annotates the table with two motifs - STAD and STHD included in the code. 
		  Based on these motifs, it decides whether or not a protein contains a Nucleolar 
		  Detention Signal("NoDS")

The following columns are added:
 protein.length	: Length of protein based on input sequences
 stad.matches	: Sequences that matched the "STAD" motif in a given protein eg: RRLAAE
 sthd.matches	: Sequences that matched the "STHD" motifs in the given protein eg:LLL,LNL
 stad.count		: How many STAD matches did the protein contain ?
 sthd.count		: How many STHD matches did the protein contain ?
 is.nods		: Based on the STAD and STHD matches, does this protein contain NoDS ?
 				  eg : 'yes' or 'no'

Usage
-----
By default, the outfile will be created in the same directory as the script.
You can change the outfile name by specifying the folder as the third input parameter

python callNoDS.py <input.txt> <output.txt> <column.with.peptide.seq>
python callNoDS.py Input/Pineiro-protein-list-with-sequences.txt Output/Pineiro-rbps-with-NoDS.txt "peptide"
==============================================================================================================

'''

# Import packages and modules
from Bio import motifs
from Bio import SeqIO
from Bio.Alphabet import IUPAC
from Bio.Alphabet.IUPAC import ExtendedIUPACProtein
from Bio.Seq import Seq
import csv
import praw
import re
import sys

# Read in text file
infile = open(sys.argv[1],'rU')
f = csv.reader(infile, delimiter='\t')
col_names = f.next()
print col_names

# Write out results
file = open(sys.argv[2],"a")
file.write('\t'.join(col_names)+"\tprotein.length\tstad.matches\tsthd.matches\tstad.count\tsthd.count\tis.nods\n")

# Find the column of the data that contains peptide sequences
seqid = col_names.index('peptide') 
gene = col_names.index(sys.argv[3])
print seqid


# Motifs used for scanning protein sequence for NoDS
stad = ['(RRL[A-Z]{3}[R]?)','(RRI[A-Z]{3}[R]?)']
sthd = ['LVL','LIL','LLL','LFL','LWL','LYL','LML','LNL','LQL','LGL','LAL','LVV','LIV','LLV','LFV','LGV','LWV','LYV','LMV','LNV','LQV','LPV']

# Looping through and looking for matches to NoDS sequence
for row in f:
	
	# Matches for STAD list above and total hits. 
	# This needs to be >=1 for the protein to qualify as containing NoDs
	stad_matches = ''
	stad_c = 0
	
	k = re.compile('|'.join(stad),re.IGNORECASE).findall(row[seqid])
	if k:
		stad_matches = [ "%s%s" % x for x in k ]
		stad_c = sum(x is not None for x in stad_matches)

	# Matches for STHD list above and total hits. 
	# This needs to be >1 for the protein to qualify as containing NoDs
	matches = [x for x in sthd if x in str(row[seqid])]
	sthd_c = sum(str(row[seqid]).count(x) for x in matches)
	
	all_match = [filter(None,stad_matches)]+matches
	print(filter(None,stad_matches),matches)
	#print(stad_c,sthd_c)
	
	nods = ""
	if stad_c>=1 and sthd_c>1:
		nods = "yes"
		#print str(row[gene])+" contains a NoDS motif"
	else: 
		nods = "no"
		#print str(row[gene])+" DOES NOT contain a NoDS motif"
	
	# Write to outfile
	file.write('\t'.join(row)+"\t"+str(len(str(row[seqid])))+"\t"+str(filter(None,stad_matches)).strip('[]')+"\t"+str(matches).strip('[]')+"\t"+str(stad_c)+"\t"+str(sthd_c)+"\t"+nods+"\n")

file.close()