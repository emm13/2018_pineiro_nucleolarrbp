#!/usr/bin/python
# Code for annotating Nucleolar RNABP with Intrinsically disordered regions

# Import packages and modules
import csv
import d2p2
import protinfo
import numpy as np
from time import gmtime, strftime

import collections
import imp
imp.reload(protinfo)
imp.reload(d2p2)


# We start by reading in the file with Uniprot IDs for Mark and David's protein list
#infile = open(sys.argv[1],'rU')
f = csv.reader(open("/Users/manasa/Documents/Work/TTT/11_NoDS-analysis/NucleolarRBP/Input/Pineiro-RBPlist.txt",'r'),delimiter="\t")
col_names = f.next()
print(col_names)

idcol = col_names.index('uniprotswissprot')
uniprot_ids = []
metadata = {}

# Save Uniprot IDs as well as metadata 
for x in f:
	#print(x)
	uniprot_ids.append(x[idcol])
	metadata[x[idcol]] = x
	
#print(uniprot_ids)
#print(metadata['P12956'])

# Declare outfile
outf = open("/Users/manasa/Documents/Work/TTT/11_NoDS-analysis/NucleolarRBP/Input/Pineiro-list-with-IDRs.tsv",'w')
outf.write('\t'.join(col_names)+"\t"+"\t".join(map(str, ("idrs", "idrs.median.consensus","overall.consensus","protein_length","total_idr_length", "fraction_idr", "number.of.idrs"))) + "\n")

# Creating IDR data containers to hold the information for proteins that do and do not have IDR annotations
idr_data_available = set()
idr_data_unavailable = set()
sequence_length_idr = {}

blocks = collections.defaultdict(list) # Blocks of consensus calls on disorder
med_consensus = collections.defaultdict(list) # Mean consensus for the blocks of consensus calls
n = 1

# Are there any tools in D2P2 that you want or do not want reported
# eg : VL-XT, VSL2b, PrDOS, PV2, Espritz and IUPred

tools_whitelist = None
tools_blacklist = None
consensus_req = 3 # Minimum number of predictors that need to agree on a base being called disordered
min_block_size = 20 # Minimum length of disordered region all of which should be called by at least 'consensus_req' number of disorder predictors


# Looping through the set of uniprot ids provided above and adding disorder annotations
# d2p2_entry is an item of class d2p2 that has the characteristics 

for d2p2_entry in d2p2.iterator(uniprot_ids):
    
    # Build the predictor list that will be used to call consensus on IDR sequence
    d2p2_entry.rebuildConsensus(
        tools_whitelist=tools_whitelist, tools_blacklist=tools_blacklist)
    
    # Set minimum peptide length and consensus support for IDRs to be called
    d2p2_entry.setIDRs(consensus_req, min_block_size)
    
    # Add the IDRs to a list
    #print(d2p2_entry.idrs)
    blocks[d2p2_entry.name] = d2p2_entry.idrs
    
    ku_con = np.array(d2p2_entry.consensus)
    g = d2p2_entry.idrs
    med = []
    
    # Calculate median consensus for each IDR in block and save it in med_consensus
    for rang in g:
    	calc = np.median(ku_con[int(rang[0]):int(rang[1])+1])
    	med.append(float("%.2f" % round(calc,2)))
 			
	med_consensus[d2p2_entry.name] = med 
    
	# Add successful entries to data available
    idr_data_available.add(d2p2_entry.name)
    sequence_length_idr[d2p2_entry.name] = d2p2_entry.length
    
    # Checking how long it takes to process requests
    n += 1
    if n%100 == 0:
        print('proteins done: %i %s' % (
                n, strftime("%Y-%m-%d %H:%M:%S", gmtime())))
    

# Add all uniprot ids without IDRs to idr_data_unavailable
idr_data_unavailable = set(uniprot_ids).difference(idr_data_available)

# Check output 457 gene symbols map to 457 Uniprot IDs
#print(list(idr_data_available)[0])
#print(blocks[list(idr_data_available)[0]])
print(len(idr_data_available)) # ID available
print(len(idr_data_unavailable))

             
# Run a test protein using d2p2 database
protein = "P12956"
print(blocks[protein])
print(med_consensus[protein])
print(sequence_length_idr[protein])

# Prints results to outfile
for protein in idr_data_available:
    total_idr = 0
    for block in blocks[protein]:
        if not block[1] - block[0] >= 20:
            print(block)
            raise ValueError()
        total_idr += (block[1] - block[0])
    	protein_length = sequence_length_idr[protein]
    	fraction_idr = total_idr/float(protein_length)
    outf.write(("\t".join(metadata[protein])+"\t"+"\t".join(map(str, (blocks[protein],med_consensus[protein],np.median(med_consensus[protein]),protein_length, total_idr, fraction_idr, len(blocks[protein]))))) + "\n")
	
outf.close()


'''
# Notes to self

The whole process starts with the statement 'for d2p2_entry in d2p2.iterator(uniprot_ids)'

1. Let's start with two Uniprot IDs, say 'P12956' & 'P13010'
2. First step is d2p2.iterator(["P12956","P13010"])
3. Refer https://github.com/TomSmithCGAT/CamProt/blob/master/camprot/proteomics/d2p2.py
4. Here, the iterator passes the ID to getChunks
	getChunks(["P12956","P13010"])

5. Get chunks breaks the list of Uniprot ids into chunks of 250, then retrieves disorder information from d2p2 website for all ids in the chunk.
		
		chunk_ids = ["P12956","P13010"]
		d2p2_url = 'http://d2p2.pro/api/seqid'
		url = '%s/["%s"]' % (d2p2_url, '","'.join(chunk_ids))
        request = http.request('GET', url=url)
        response = json.loads(request.data.decode('utf8'))
        
        Alternatively in ipython:
        	import urllib.request
			request = urllib.request.urlopen(url).read()
        	response = json.loads(request.decode('utf8'))
       	    yield response
6. So the iterator calls getChunks on a bunch of IDs and is returned a dictionary with protein id as key and with several values as a list. The values are
{ 	Uniprot ID : "P13010"
	Uniprot version : "Uniprot 2017_06 genome"
	Structure : {"structure":{"pfam":[["Domain","CL0128","PF03731.10","Ku_N","Ku70\/Ku80 N-terminal alpha\/beta domain","6.499999999999999e-75","251.3","37","256"],
				["Domain","No_clan","PF02735.11","Ku","Ku70\/Ku80 beta-barrel domain","1.9000000000000002e-57","193.9","261","463"],
				["Family","No_clan","PF03730.9","Ku_C","Ku70\/Ku80 C-terminal arm","4.0000000000000003e-29","101","469","559"],
				["Family","CL0306","PF02037.22","SAP","SAP domain","0.00000001","34.4","573","607"]],
				"strong":[["SPOC domain-like","100939","6.08e-86","Ku70 subunit middle domain","100940","0.000000000136","254-533"],
				["vWA-like","53300","6.67e-62","Ku70 subunit N-terminal domain","100959","0.00000000242","34-253"],
				["SAP domain","68906","0.00000000000061","SAP domain","68907","0.0000763","560-608"]],"weak":[]}
	Disorder :  {"disorder":{"conflict":[0,0,0,0,0,0,0,0,0],"consensus":[6,6,6,6,6,3,4,1,0],"consranges":[["1","1"],["7","28"],["177","185"],["536","558"]],"disranges":[["VLXT","1","1"],["VSL2b","1","40"]]}}
		The disorder sequence for each amino acid in the protein sequence : 0 = none, 1 = one predictor called disorder, 2,3,4....predictors called disorder. We want at least 3 to call disorder
		conflict has 0 when there is no conflict between predictors; consensus when there is agreement between predictors and number indicates number of predictors; consranges = ranges where the predictors agree i.e most confident disordered regions; 
		disranges are list of lists of ranges for each predictor where that predictor thinks there is a disordered peptide sequence.
}
7. Response contains the above for every uniprot ID. Now the task is to unscramble it all
8. This is done by this loop
 
	for key in chunk:
		d2p2_text = chunk[key]
		if len(d2p2_text) > 0:
			yield d2p2(chunk[key][0]) <- This is where the d2p2 instance is initiated

9. This means that 'd2p2_entry' equals each entry in 'd2p2_text' which itself is an object of Class 'd2p2' with the features in step 6. above

10. The main features in a 'd2p2' object are

		self.name = d2p2_text[0] # Uniprot ID
        annotations_dict = d2p2_text[2] # Structure dictionary 
        self.disranges = annotations_dict['disorder']['disranges']
        self.consensus = annotations_dict['disorder']['consensus']
        self.structure = annotations_dict['structure']
        self.length = len(self.consensus) 
        self.idrs = [] # To be defined

11. Start by filtering the consensus calls to only include data from those predictors that we are interested in. For our purposes, we keep all of them. 
			d2p2_entry.rebuildConsensus(
        	tools_whitelist=tools_whitelist, tools_blacklist=tools_blacklist)
        	
12. Once we are happy with the preditor list, we note that self.idrs is empty and need to fill it. This is done by the function 'setIDRs' which iterates through each member of 'self.consensus'
			
			for position, cons in enumerate(self.consensus):
            
            # identify where consensus is too low
            if cons < consensus_req:
                if position - start >= min_block_size:
                    blocks.append((start, position))
                start = position + 1

        if position - start >= min_block_size:
            blocks.append((start, position + 1))

        self.idrs = blocks
 
13. We assign the list of idrs to a dictionary where key is the Uniprot ID and values are a list of consensus IDR locations
		blocks[d2p2_entry.name] = d2p2_entry.idrs
14. We add those proteins with idrs to the idr_data_available set
		idr_data_available.add(d2p2_entry.name)
15. We add sequence length to the entry
		sequence_length_idr[d2p2_entry.name] = d2p2_entry.length
16. Finally, we itirate through all proteins that have idrs and print them to the outfile
	
	
''' and None
















