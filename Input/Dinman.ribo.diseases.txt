Protein	Disease	Subunit	Disease.type
RPL11	T-ALL, Diamond.Blackfan.Anaemia	Large	Both
RPL5	Multiple.cancers, Diamond.Blackfan.Anaemia	Large	Both
RPS27	Melanoma, Diamond.Blackfan.Anaemia	Small	Both
RPL10	T-ALL	Large	Cancer
RPL22	T-ALL	Large	Cancer
RPL23A	Endometrioid.cancer	Large	Cancer
RPS15	CLL	Small	Cancer
RPS20	Colorectal.cancer	Small	Cancer
RPSA	Gastric.cancer	Small	Cancer
RPL15	Diamond.Blackfan.Anaemia	Large	Ribosomopathies
RPL26	Diamond.Blackfan.Anaemia	Large	Ribosomopathies
RPL31	Diamond.Blackfan.Anaemia	Large	Ribosomopathies
RPL35A	Diamond.Blackfan.Anaemia	Large	Ribosomopathies
RPS10	Diamond.Blackfan.Anaemia	Small	Ribosomopathies
RPS14	5q.MDS	Small	Ribosomopathies
RPS17	Diamond.Blackfan.Anaemia	Small	Ribosomopathies
RPS19	Diamond.Blackfan.Anaemia	Small	Ribosomopathies
RPS24	Diamond.Blackfan.Anaemia	Small	Ribosomopathies
RPS26	Diamond.Blackfan.Anaemia	Small	Ribosomopathies
RPS28	Diamond.Blackfan.Anaemia	Small	Ribosomopathies
RPS29	Diamond.Blackfan.Anaemia	Small	Ribosomopathies
RPS7	Diamond.Blackfan.Anaemia	Small	Ribosomopathies